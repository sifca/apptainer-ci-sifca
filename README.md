## Name
apptainer-ci

## Description
This project creates a Docker image that can be used in the GitLab CI flow to create Apptainer images.

## Usage
You can use this image with your .gitlab-ci.yml files or in a Apptainer/Singularity definition file.

#### .gitlab-ci.yml
```
image:
  name: gitlab-registry.oit.duke.edu/oit-dcc/apptainer-ci/apptainer-ci:latest
  entrypoint: ["/bin/sh", "-c"]
```

#### Apptainer.def
BootStrap: docker
From: gitlab-registry.oit.duke.edu/oit-dcc/apptainer-ci/apptainer-ci:latest
