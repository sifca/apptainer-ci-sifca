FROM ubuntu:22.04
RUN apt update && \
    apt install -y software-properties-common && \
    add-apt-repository -y ppa:apptainer/ppa
RUN apt update && \
    apt install -y apptainer apptainer-suid
ENTRYPOINT ["/bin/sh"]
